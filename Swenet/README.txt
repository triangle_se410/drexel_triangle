SWENET 9.3 Migration README


Documentation -
The following is a list of each document contained in the Swenet.zip file and a short description of each.  The list is a suggested order of reading and each description will express the document's significance for migrating SWENET.

"SWENET Data Flow.doc"
A very brief document with a very high-level overview of the way information interacts in the SWENET system.

"Getting Up to Speed.doc"
Contains an introduction to the technologies used in the SWENET web system.  It was originally intended as a document used for new co-ops starting work on the project, but it can be used to gain insight on the configurations of each individual component of the system.

"Swenet Architecture.doc"
A comprehensive review of the SWENET infrastructure.  This document describes key components within the code that make SWENET work the way it does.  While it may not be necessary for migrating the system, it does contain an overview of the file system arrangement of the source files themselves.

"Deployment Documentation.doc"
SWENET site deployment manual with step-by-step details.  Follow this document precisely when releasing the site onto the production server.  Some details may not apply for various reasons, such as the development and production server being the same machine.

"SWENET Releases.doc"
Partitioned by each consecutive release, this document provides a timeline of updates and changes to the website.  In no way technical, it mostly contains the new feature descriptions sent out to subscribers at the time of a new release.

"Release 7 - Module Versioning.doc"
"Release 8 - Justin.doc"
"Release 8.1 - Dan.doc"
In-depth release documents specifying the exact functionality and authors of each.

"Project Evolution.doc"
A technical summary of the newest features introduced to the site as further development since release 8.1.  This describes the key concepts and their implementations that were major issues to the developers during the summer of 2005.

"DotWiki Integration.doc"
This is a thorough technical review of the aquisition, implementation, and further development of the wiki engine incorporated into the SWENET website.  One of the final stages of the SWENET development, this document should be sufficient if any further functionality is added to the wiki side of the site.

"Module Submission Tutorial.doc"
Meant entirely as a user document, this tutorial explains in detail the process and purpose of filling out the forms for submitting a module to the site itself.  Reading this document may provide a more thorough understanding of the motivation behind the chosen submission technique.

"TestVirus.txt"
A simple text file containing the string that will always cause virus scanning software to specify the file as hosting a virus.