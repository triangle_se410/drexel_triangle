SQLAlchemy database 
	> Using a SQLite database
	> Will be a file on filesystem as site.db

1. Open command line and navigate to project directory
2. Install Flask-SQLAlchemy package
	> pip3 install flask-sqlalchemy
	> pip3 install flask_bcrypt
	> pip3 install flask-login
	> pip3 install flask_wtf
3. Open python interpreter
	> python3
4. Import database instance
	> from swenet import db
5. Create database
	> db.create_all()
6. Import models
	> from swenet.models import User
7. Various commands
	> User.query.all()
	> User.query.first() 	# Queries first item in database
	> User.query.filter_by(user='USERNAME').all() # Queries database by filter
	> db.drop_all()		# Delete all data in database